/**
 * @file
 * Attaches behavior for the Filter Tooltips module.
 */

(function (Drupal) {
  'use strict';

  /**
   * Displays the tooltips of the selected text format automatically.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behavior for updating filter tooltips.
   */
  Drupal.behaviors.filterTooltips = {
    attach: function attach(context, settings) {
      const $tooltips = once(
        'filter-tooltips',
        '.filter-tooltips',
        context.parentNode || context,
      );
      $tooltips.forEach(($tooltip) => {
        if (settings.filter_tooltips.trigger_event == 'mouseover') {
          $tooltip.addEventListener('mouseover', (event) => {
            toggleExplanation($tooltip, event)
          });
          $tooltip.addEventListener('mouseout', (event) => {
            toggleExplanation($tooltip, event)
          });
        }
        else {
          $tooltip.addEventListener(settings.filter_tooltips.trigger_event, (event) => {
            toggleExplanation($tooltip, event)
          });
        }
      });

      const toggleExplanation = function($tooltip, event) {
        event.preventDefault();

        const $explanation = $tooltip.getElementsByClassName('filter-tooltips-explanation')[0];
        if (typeof $explanation === 'undefined') {
          const $explanation = document.createElement('div');
          $explanation.classList.add('filter-tooltips-explanation', 'open');
          $explanation.innerHTML = $tooltip.dataset.explanation;
          $tooltip.appendChild($explanation);
        }
        else {
          $explanation.classList.toggle('open');
        }
      }
    }

  };
})(Drupal, once);

