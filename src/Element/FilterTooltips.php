<?php

namespace Drupal\filter_tooltips\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Url;

/**
 * Provides a form element for filter_tooltips.
 *
 * @FormElement("filter_tooltips")
 */
class FilterTooltips extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#process' => [
        [$class, 'processFilterTooltipsAutocomplete'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderFilterTooltipsElement'],
        [$class, 'preRenderGroup'],
      ],
      '#theme' => 'input__textfield',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    return Textfield::valueCallback($element, $input, $form_state);
  }

  /**
   * Adds filter_tooltips custom autocomplete functionality to elements.
   *
   * Instead of using the core autocomplete, we use our own.
   *
   * {@inheritdoc}
   *
   * @see \Drupal\Core\Render\Element\FormElement::processAutocomplete
   */
  public static function processFilterTooltipsAutocomplete(&$element, FormStateInterface $form_state, &$complete_form) {
    $url = NULL;
    $access = FALSE;
    if (!empty($element['#autocomplete_route_name'])) {
      $parameters = $element['#autocomplete_route_parameters'] ?? [];
      $url = Url::fromRoute($element['#autocomplete_route_name'], $parameters);
      $access = $url->access(\Drupal::currentUser());
    }

    $metadata = BubbleableMetadata::createFromRenderArray($element);
    if ($access) {
      $element['#attributes']['class'][] = 'form-filter-tooltips-autocomplete';
      $metadata->addAttachments(['library' => ['filter_tooltips/filter_tooltips.autocomplete']]);
      // Provide a data attribute for the JavaScript behavior to bind to.
      $element['#attributes']['data-autocomplete-path'] = $url->toString(TRUE)->getGeneratedUrl();

      $metadata
        ->merge($url->toString(TRUE))
        ->merge(BubbleableMetadata::createFromObject($access))
        ->applyTo($element);
    }
    return $element;
  }

  /**
   * Prepares a #type 'filter_tooltips' render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderFilterTooltipsElement(array $element) {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, ['id', 'name', 'value', 'size']);
    static::setAttributes($element, ['form-text']);

    return $element;
  }

}
