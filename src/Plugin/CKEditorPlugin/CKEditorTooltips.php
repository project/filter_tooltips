<?php

namespace Drupal\filter_tooltips\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Filter Tooltips" plugin.
 *
 * @CKEditorPlugin(
 *   id = "filter_tooltips",
 *   label = @Translation("Filter Tooltips"),
 *   module = "filter_tooltips"
 * )
 */
class CKEditorTooltips extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    if ($library_path = \Drupal::service('extension.list.module')->getPath('filter_tooltips')) {
      return $library_path . '/js/plugin.js';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'filter_tooltips' => [
        'label' => $this->t('Filter Tooltips'),
        'image' => \Drupal::service('extension.list.module')->getPath('filter_tooltips') . '/js/icons/filter_tooltips.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'filter_tooltips_dialogTitleAdd' => $this->t('Add tooltip'),
      'filter_tooltips_dialogTitleEdit' => $this->t('Edit tooltip'),
    ];
  }

}
