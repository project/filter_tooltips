<?php

namespace Drupal\filter_tooltips\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller class for the filter_tooltips module.
 */
class AutocompleteController implements ContainerInjectionInterface {

  /**
   * Constructs a AutocompleteController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type manager service.
   */
  public function __construct(protected Connection $database, protected EntityTypeManagerInterface $entityTypeManager) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Menu callback for filter_tooltips search autocompletion.
   *
   * Like other autocomplete functions, this function inspects the 'q' query
   * parameter for the string to use to search for suggestions.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $filter_id
   *   The filter id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions.
   */
  public function autocomplete(Request $request, $filter_id) {
    $string = mb_strtolower($request->query->get('q'));
    $match = $this->database->escapeLike($string);

    $entity_type = $this->entityTypeManager->getDefinition('taxonomy_term');
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $label_key = $entity_type->getKey('label');

    if ($label_key) {
      $query->condition($label_key, '%' . $match . '%', 'LIKE');
      $query->sort($label_key, 'ASC');
    }

    // Vocabulary check.
    $config = \Drupal::config('filter.format.' . $filter_id);
    $vocabulary = $config->getRawData()['filters']['filter_tooltips']['settings']['filter_tooltips_vocabulary'];
    if (!empty($vocabulary)) {
      $query->condition('vid', $vocabulary);
    }

    // Add access tag for the query.
    $query->addTag('entity_access');
    $query->addTag('taxonomy_term_access');

    $result = $query->execute();

    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($result);
    $matches = [];
    foreach ($terms as $term) {
      $matches[] = [
        "name" => $term->getName(),
        "description" => $term->getDescription(),
      ];
    }
    $json_object = new \stdClass();
    $json_object->matches = $matches;

    return new JsonResponse($json_object);
  }

}
