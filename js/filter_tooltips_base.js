/**
 * @file
 * Some basic behaviors and utility functions for Filter Tooltips.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * @namespace
   */
  Drupal.filter_tooltips = {};

})(jQuery, Drupal, drupalSettings);
