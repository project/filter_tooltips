<?php

namespace Drupal\filter_tooltips\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\filter\Entity\FilterFormat;

/**
 * Provides a Filter Tooltips dialog for text editors.
 */
class FilterTooltipsEditorDialog extends FormBase {

  /**
   * The editor storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $editorStorage;

  /**
   * Constructs a form object for filter_tooltips dialog.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $editor_storage
   *   The editor storage service.
   */
  public function __construct(EntityStorageInterface $editor_storage) {
    $this->editorStorage = $editor_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('editor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filter_tooltips_editor_dialog_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {
    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    $user_input = $form_state->getUserInput();
    $input = $user_input['editor_object'] ?? [];

    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="filter-tooltips-editor-dialog-form">';
    $form['#suffix'] = '</div>';

    $form['attributes']['name'] = [
      '#title' => $this->t('Find tooltip'),
      '#type' => 'filter_tooltips',
      '#default_value' => $input['name'] ?? '',
      '#description' => $this->t('Start typing to find a tooltip.'),
      '#autocomplete_route_name' => 'filter_tooltips.autocomplete',
      '#autocomplete_route_parameters' => [
        'filter_id' => $filter_format->id(),
      ],
      '#weight' => 0,
    ];

    $this->addAttributes($form, $form_state, $input);

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $attributes = array_filter($form_state->getValue('attributes'));
    $form_state->setValue('attributes', $attributes);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#filter-tooltips-editor-dialog-form', $form));
    }
    else {
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Adds the attributes enabled on the current profile.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $input
   *   An array with the attribute values from the editor.
   */
  private function addAttributes(array &$form, FormStateInterface &$form_state, array $input) {
    $form['filter_tooltips_attributes'] = [
      '#type' => 'container',
      '#title' => $this->t('Attributes'),
      '#weight' => '10',
    ];
    $form['filter_tooltips_attributes']['description'] = [
      '#type' => 'hidden',
      '#default_value' => $input['description'] ?? '',
    ];
  }

}
